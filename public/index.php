<?php

header('Content-Type: text/html; charset=UTF-8');

$errors = [];
$trimed=[];
$result_message = array();



if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (!empty($_COOKIE['save'])) {
        setcookie('save', '', 100000);
        setcookie('login', '', 100000);
        setcookie('pass', '', 100000);
        $result_message[] ='Спасибо, результаты сохранены.';
        if (!empty($_COOKIE['pass'])) {
            $result_message[] = sprintf('Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
              и паролем <strong>%s</strong> для изменения данных.',
              strip_tags($_COOKIE['login']),
              strip_tags($_COOKIE['pass']));
        } 
    }

    

    $errors['name'] = !empty($_COOKIE['name_error']);
    if($errors['name']){
        setcookie('name_error', '', 100000);
        $result_message[] = '<div> Заполните имя.</div>';
    }

    $errors['email'] = !empty($_COOKIE['email_error']);
    if($errors['email']){
        setcookie('email_error', '', 100000);
        $result_message[] = '<div> Заполните почту.</div>';
    }

    $errors['god'] = !empty($_COOKIE['god_error']);
    if($errors['god']){
        setcookie('god_error', '', 100000);
        $result_message[] = '<div> Заполните дату рождения.</div>';
    }

    $errors['sex'] = !empty($_COOKIE['sex_error']);
    if($errors['sex']){
        setcookie('sex_error', '', 100000);
        $result_message[] = '<div> Выберите ваш пол.</div>';
    }

    $errors['limb'] = !empty($_COOKIE['limb_error']);
    if($errors['limb']){
        setcookie('limb_error', '', 100000);
        $result_message[] = '<div> Выберите количество конечностей.</div>';
    }

    $errors['ch'] = !empty($_COOKIE['ch_error']);
    if($errors['ch']){
        setcookie('ch_error', '', 100000);
        $result_message[] = '<div> Вы должны быть согласны с условиями.</div>';
    }



//Создаем массив значений полей и перепишем значения из формы
    $values = array();
    $values['name'] = empty($_COOKIE['value_of_name']) ? '' : $_COOKIE['value_of_name'];
    $values['email'] = empty($_COOKIE['value_of_email']) ? '' : $_COOKIE['value_of_email'];
    $values['god'] = empty($_COOKIE['value_of_god']) ? '' : $_COOKIE['value_of_god'];
    $values['sex'] = empty($_COOKIE['value_of_sex']) ? '' : $_COOKIE['value_of_sex'];
    $values['limb'] = empty($_COOKIE['value_of_limb']) ? '' : $_COOKIE['value_of_limb'];
    $values['ch'] = empty($_COOKIE['value_of_ch']) ? '' : $_COOKIE['value_of_ch'];
    $values['superpower'] = array();
    $values['superpower'][0] = empty($_COOKIE['superpower_0']) ? '' : $_COOKIE['superpower_0'];
    $values['superpower'][1] = empty($_COOKIE['superpower_1']) ? '' : $_COOKIE['superpower_1'];
    $values['superpower'][2] = empty($_COOKIE['superpower_2']) ? '' : $_COOKIE['superpower_2'];

    session_start();
    if (empty($errors) && !empty($_COOKIE[session_name()]) && !empty($_SESSION['login'])) {
        // TODO: загрузить данные пользователя из БД
        // и заполнить переменную $values,
        // предварительно санитизовав.
        $user = 'u41010';
        $pass = '4436326';
        $bd = new PDO('mysql:host=localhost;dbname=u41010', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
    
        $tab1= $bd->prepare('SELECT name, email, god, sex, limb, bio FROM form WHERE user_id = ?');
        $tab1->execute([$_SESSION['uid']]);
        $row = $tab1->fetch(PDO::FETCH_ASSOC);
        $values['name'] = strip_tags($row['name']);
        $values['email'] = strip_tags($row['email']);
        $values['god'] = strip_tags($row['god']);
        $values['sex'] = strip_tags($row['sex']);
        $values['limb'] = strip_tags($row['limb']);
        $values['bio'] = strip_tags($row['bio']);

        $tab2 = $db->prepare('SELECT id_sup FROM user_sup WHERE user_id = ?');
        $tab2->execute([$_SESSION['uid']]);
        while($row = $tab2->fetch(PDO::FETCH_ASSOC)) {
          $values['superpower'][$row['id_sup']] = TRUE;
        }
    
        printf('Вход с логином %s, uid %d', $_SESSION['login'], $_SESSION['uid']);
    }

    include('form.php');
}

// Проверка на корректность заполнения
else {
    $errors = false;

    if(empty($_POST['name'])){
        setcookie('name_error', '1', time() + 24*60*60);
        $errors = true;
    }
    else{
        setcookie('value_of_name', $_POST['name'], time() + 30*24*60*60);
    }
    
    if(!preg_match('/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/', $_POST['email'])){
        setcookie('email_error', '1', time() + 24*60*60);
        $errors = true;
    }
    else{
        setcookie('value_of_email', $_POST['email'], time() + 30*24*60*60);
    }

    if (!preg_match('/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/', $_POST['god'])){
        setcookie('god_error', '1', time() + 24*60*60);
        $errors = true;
    }else{
        setcookie('value_of_god', $_POST['god'], time() + 30*24*60*60);
    }

    if (!preg_match('/^[0-1]$/', $_POST['sex'])) {
        setcookie('sex_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
      }
      else {
        setcookie('value_of_sex', $_POST['sex'], time() + 30 * 24 * 60 * 60);
      }

    if (!preg_match('/^[0-4]$/', $_POST['limb'])) {
    setcookie('limb_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
    }
    else {
        setcookie('value_of_limb', $_POST['limb'], time() + 30 * 24 * 60 * 60);
    }

    if (!isset($_POST['ch'])) {
        setcookie('ch_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
      }
      else {
        setcookie('value_of_ch', $_POST['ch'], time() + 30 * 24 * 60 * 60);
      }

      foreach($_POST['superpower'] as $sup) {
        setcookie('superpower_value_' . $sup, 'true', time() + 30 * 24 * 60 * 60);
        }
    
        if ($errors) {
            header('Location: index.php');
            exit();
        }
    
        else{
            setcookie('name_error', '', 100000);
            setcookie('email_error', '', 100000);
            setcookie('sex_error', '', 100000);
            setcookie('limb_error', '', 100000);
            setcookie('god_error', '', 100000);
            setcookie('ch_error', '', 100000);
        }

        // Проверяем меняются ли ранее сохраненные данные или отправляются новые.
    if (!empty($_COOKIE[session_name()]) && session_start() && !empty($_SESSION['login'])) {
        // TODO: перезаписать данные в БД новыми данными,
        $user = 'u41010';
        $pass = '4436326';
        $db = new PDO('mysql:host=localhost;dbname=u41010', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

        $tab1 = $db->prepare("UPDATE form SET name = ?, email = ?, god = ?, sex= ? , limb = ?, bio = ?  WHERE user_id = ?");
        $tab1 -> execute([$_POST['name'], $_POST['email'], $_POST['god'], $_POST['sex'], $_POST['limb'], $_POST['bio'], $_SESSION['uid']]);

        $tab2 = $db->prepare('DELETE FROM user_sup WHERE user_id = ?');
        $tab2->execute([$_SESSION['uid']]);

        $lastId = $_SESSION['uid'];
        $tab3 = $db->prepare("INSERT INTO user_sup SET user_id = ?, id_sup = ?");
        foreach ($_POST['superpower'] as $sup) $tab3 -> execute([$lastId, $sup]);


        setcookie('value_of_name', '', 100000);
        setcookie('value_of_email', '', 100000);
        setcookie('value_of_god', '', 100000);
        setcookie('value_of_sex', '', 100000);
        setcookie('value_of_limb', '', 100000);
        setcookie('value_of_ch', '', 100000);
        header('Location: login.php');
    }
    else{
        // Генерируем уникальный логин и пароль.
        // TODO: сделать механизм генерации, например функциями rand(), uniquid(), md5(), substr().
        $id = uniqid();
        $hash = md5($id);
        $login = substr($hash, 0, 10);
        $pass = substr($hash, 10, 15);
        $hash_pass = substr(hash("sha256", $pass), 0, 20);
        // Сохраняем в Cookies.
        setcookie('login', $login);
        setcookie('pass', $pass);
    
         $user = 'u41010';
         $pass_db = '4436326';
         $db = new PDO('mysql:host=localhost; dbname=u41010', $user, $pass_db, array(PDO::ATTR_PERSISTENT => true));
  
  
         $tab1 = $db->prepare("INSERT INTO form SET name = ?, email = ?, god = ?, sex= ? , limb = ?, bio = ?, login = ?, hash_pass = ?");
         $tab1 -> execute([$_POST['name'], $_POST['email'], $_POST['god'], $_POST['sex'], $_POST['limb'], $_POST['bio'], $login, $hash_pass]);
         $tab2 = $db->prepare("INSERT INTO user_sup SET user_id = ?, id_sup = ?");
         $id = $db->lastInsertId();
         foreach ($_POST['superpower'] as $sup) $tab2 -> execute([$id, $sup]);
      }

        setcookie('value_of_name', '', 100000);
        setcookie('value_of_email', '', 100000);
        setcookie('value_of_god', '', 100000);
        setcookie('value_of_sex', '', 100000);
        setcookie('value_of_limb', '', 100000);
        setcookie('value_of_ch', '', 100000);
        setcookie('save', '1');
        header('Location: ./');
    }
?>