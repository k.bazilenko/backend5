<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style1.css">
    </head>

  <body>
    
    <div class="main">
    <section id="form">
    <h2>Авторизация</h2>

<?php

/**
 * Файл login.php для не авторизованного пользователя выводит форму логина.
 * При отправке формы проверяет логин/пароль и создает сессию,
 * записывает в нее логин и id пользователя.
 * После авторизации пользователь перенаправляется на главную страницу
 * для изменения ранее введенных данных.
 **/

// Отправляем браузеру правильную кодировку,
// файл login.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// Начинаем сессию.
session_start();

// В суперглобальном массиве $_SESSION хранятся переменные сессии.
if (!empty($_SESSION['login'])&&$_GET['quit']) {
  // Если есть логин в сессии, то пользователь уже авторизован.
  // TODO: Сделать выход (окончание сессии вызовом session_destroy()
  //при нажатии на кнопку Выход).
  // Делаем перенаправление на форму.
  session_destroy();
  header('Location: index.php');
}



// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (!empty($_GET['nologin']))
    print("<div>Пользователя с таким логином не существует</div>");
  if (!empty($_GET['wrongpass']))
    print("<div>Неверный пароль!</div>");


?>

  <div class="login-page">
        <div class="form">
          <form class="login-form" action="" method="post">
            <input class="input-field" name="login"  placeholder="логин"/>
            <input class="input-field" name="pass" placeholder="пароль"/>
            <input class="gradient-button" type="submit" value="войти">
          </form>
        </div>
  </div>


<?php
}
// Иначе, если запрос был методом POST, т.е. нужно сделать авторизацию с записью логина в сессию.
else {
  $user = 'u41010';
  $pass_bd = '4436326';
  $db = new PDO('mysql:host=localhost; dbname=u41010', $user, $pass_bd, array(PDO::ATTR_PERSISTENT => true));

  $tab1 = $db->prepare('SELECT  user_id, hash_pass FROM form WHERE login = ?');
  $tab1->execute([$_POST['login']]);

  $row = $tab1->fetch(PDO::FETCH_ASSOC);
  if (!$row) {
    header('Location: ?nologin=1');
    exit();
  } 
  
  $pass_hash = substr(hash("sha256", $_POST['pass']), 0, 20);
  if ($row['hash_pass'] != $pass_hash) {
    header('Location: ?wrongpass=1');
    exit();
  }

  $_SESSION['login'] = $_POST['login'];
  // Записываем ID пользователя.
  $_SESSION['uid'] = $row['user_id'];

  // Делаем перенаправление.
  header('Location: ./');
}
?>

</section>
</div>
</body>
</html>
