
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Задание 5</title>
</head>

<body>

    <?php
	    if (!empty($result_message)) {
	        print('<div id="messages">');
	        foreach ($result_message as $result_message) {
		        print($result_message);
	        }
	        print('</div>');
	      }
    ?>


    <div class="main">
    <div class="block_4">
    <?php 
              if(!empty($_COOKIE[session_name()]) && !empty($_SESSION['login']))
                  print('<h1 id="form"> Форма(режим редактирования) </h1>');
              else
                  print('<h1 id="form"> Форма </h1>');
          ?>

        <form method="POST" id="form">
            <label>Имя:<br />
                <input type=text name="name" placeholder="Введите ваше имя" 
                <?php if($errors['name']){print 'class = "error"';}?> 
                value="<?php print $values['name']; ?>" />
            </label><br />

            <label>E-mail:<br />
                <input name="email" placeholder="Введите ваш e-mail" type="email"
                <?php if($errors['email']){print 'class = "error"';}?>
                value="<?php print $values['email']; ?>">
            </label><br />

            <label>Дата рождения:<br />
            <input type="date" name="god" <?php if($errors['god']){print 'class = "error"';}?>>
            </label><br />

            <label>Пол:</label><br />
            <label class="radio"><input type="radio" checked="checked" name="sex" value=1 <?php if ($values['sex'] == '1') {print 'checked';} ?> />Мужской
            </label>
            <label class="radio"><input type="radio" name="sex" value=0 <?php if ($values['sex'] == '0') {print 'checked';} ?>/>Женский
            </label><br />

            <label>Выберите кол-во конечностей:</label><br />
            <label class="radio"><input type="radio" checked="checked" name="limb" value=0 <?php if ($values['limb'] == '0') {print 'checked';} ?>/>0
            </label>
            <label class="radio"><input type="radio" name="limb" value=1 <?php if ($values['limb'] == '1') {print 'checked';} ?>/>1
            </label>
            <label class="radio"><input type="radio" name="limb" value=2 <?php if ($values['limb'] == '2') {print 'checked';} ?>/>2
            </label>
            <label class="radio"><input type="radio" name="limb" value=3 <?php if ($values['limb'] == '3') {print 'checked';} ?>/>3
            </label>
            <label class="radio"><input type="radio" name="limb" value=4 <?php if ($values['limb'] == '4') {print 'checked';} ?>/>4
            </label><br />

            <label>Ваши сверхспособности:<br />
                <select multiple="true" name="superpower[]">
                    <option value="Бессмертие" <?php if ($values['superpower']['0']) {print 'selected';} ?>>Бессмертие</option>
                    <option value="Прохождение сквозь стены" <?php if ($values['superpower']['0']) {print 'selected';} ?>>Прохождение сквозь стены</option>
                    <option value="Левитация" <?php if ($values['superpower']['0']) {print 'selected';} ?>>Левитация</option>
                </select>
            </label><br />

            <label>
                Биография:<br />
                <textarea name="bio" placeholder="Расскажите о себе"></textarea>
                <br />
            </label>

            <label <?php if ($errors['ch']) {print 'class="error"';} ?>>
                <input name="ch" type="checkbox" checked=checked value=1>С контрактом ознакомлен:<br />
            </label>

            <input type="submit" value="Отправить" />
        
    </div>
    </form>
</div>
</div> 
  

<nav id="nav" style="text-align: center">
        <ul>
          <li><?php 
              if(!empty($_COOKIE[session_name()]) && !empty($_SESSION['login']))
                  print('<a href="login.php?quit=1" class = "gradient-button" title = "Выйти">Выйти</a>');
              else
                  print('<a href="login.php" class = "gradient-button"  title = "Войти">Войти</a>');
          ?></li>
        </ul>
      </nav>
       
   
</body>

</html>

